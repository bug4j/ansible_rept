Доклад на тему "Управление конфигурацией. Ansible."

1. Вступление.

	1.1. Оч кратко о себе и о компании.
	
	Я работаю в компании Maground (maground.com). 
	Компания специализируется на продаже профессиональных фотографий высокого качества.
	Мы в компании разрабатываем разнообразные сервисы.
	Например для конвертирования и публикации фотографий, для отправки инвойсов.

	1.2. Упавление конфигурацией (УК) -- что это и какие проблемы решает? IAC кратко.
	
	Программирование без оглядки на окружение в котором будет работать Ваше приложение не возможно.
	В настоящее время команда разработки вовлечена в процесс управления окружением.
	
	//Разработчики конечно могут помочь себе выбрав абстракцию в виде Docker если
	//это позволяет разрабатываемое ими приложение и есть догово
	К системным администраторам предъявляют повышенные требования. 
	     
	Системное администрирование 
	УК сейчас является очень важным компонентом. 

	1.3. Для чего мы у себя используем?
	
	    У нас есть конфигурации для:
	    * провайзинг/реконфигурация тестовых стендов
	    * провайзинг/реконфигурация прод стендов
	    * провайзинг/реконфигурация стенда с Jenkins
	    * провайзинг/реконфигурация стенда мониторинга (Prometheus+Grafana+Alertmanager) и экспортеров
	    * провайзинг/реконфигурация Graylog и Logspout
	    
	    Раньше было:
	    Bash скрипты для развертывания приложения.
	    Всего остального либо не было либо выполнялось вручную. Многое и сейчас выполняется вручную.

	1.4. Инструменты автоматизации инфраструктуры: 

		- скрипты bash
		- Ansible, Chif, Salt, Puppet

2. Основная часть.

	2.1. Подробнее про Ansible:

    - кто поддерживает/владеет
		C 2015 года владеет и поддерживает это проект комания Red Hat, Inc.

    - взаимодействие с целевыми нодами. (ssh, Python, Уточнить как работать с windows)
    Транспорт: ssh, psrp, saltstack (всего около 20)
    A не требует установки клиентской части на управляемую машину.
    Для соединения используется чаще SSH. 
    Но список поддерживаемых транспортов велик.
    Около 20. Среди которых хотелось бы отметить:
        - psrp - PowerShell Remoting Protocol 
        - winrm – имплементация MS протокола WS-Management ( Web Services for Management )
        - saltstack - используется для работы с миньонами saltstack
        Есть еще хитрые примочки для запуска команд в контейнерах и подах, в ВМ lxc
		
    Как выглядит. yaml, плейбуки, роли, инвентарные файлы, темплейты
    
    Ansible проверяет, что каждый из ресурсов системы находится в ожидаемом состоянии 
    и пытается исправить состояние ресурса, если оно не соответствует ожидаемому.
    
    Требуемое состояние описывается либо в простых сценариях -- плейбуках, либо в ролях. 
    Ансибл описывает конфигурацию декларативно. Те как.
    Т.е. мы описываем как должна выглядеть инфраструктура. 
    Какие пакеты должны быть установлены или обновлены, 
        какие сервисы проинсталлированы или запущены,
        какие файлы должны как выглядеть,
        выполнение отдельных команд в консоли.
    Важная черта сценариев ansible Идемпоте́нтность. 
    Сколько бы раз вы не выполнили сценарий система должна остаться в описанной конфигурации.
    
    Мы можем желаемое состояние описать в одном или нескольких сценариях.
    Это достаточно удобно когда мы делаем какую-то специфическую конфигурацию.
    TODO: придумать пример конфигурации
    
    Но если мы хотим использовать какую-то типовую конфигурацию, 
    например разверачивать на некоторых нодах Docker.
    То тут будет удобно выделить отдельный сценарий установки в Роль. 
    Роль это еще одна сущность которой оперируют в Ansible.
    Роль вы можете написать сами, а можете взять уже готовую, написанную кем-то.
    Для ролей есть ресурс galaxy.ansible.com.
        
    Плагины они же модули: несколько примеров кода
    Для работы с отдельными состояниями используются модули ансибл.
    Запуск модулей описывается в плейбуках.
        Например: 
        
        - name: Unarchive a file that needs to be downloaded (added in 2.0)
                  unarchive:
                    src: https://example.com/example.zip
                    dest: /usr/local/bin
                    remote_src: yes
        
	2.2. Ansible в действии

		- нужно будет подготовить сценарии для быстрого развертывания: 

			- Vagrant, три виртуалки: мастер, бэкенд и фронтенд

			- Роли Ansible для применения на виртуалках

			- Кейсы использования

	2.3. Вкусности Ansible:

    - готовые роли Ansible (https://galaxy.ansible.com)

    - поддержка в IDE: VS Code есть плагин для поддержки Ansible от MS

    - тестирование: молекула https://github.com/ansible/molecule
    
        Я уверен что все здесь собравшиеся добросовестные разработчики.
        А добросовестные разработчики всегда все тестируют. 
        Для тестирования ролей ансибле есть инструмент Molecule.
        Принцип тестирования заключается в том что роль накатывается на Docker контейнер,
        а затем выполняются тесты над этой ролью.
        Докер не обязательное условие. Можно использовать и облачные сервисы Azure, EC2, GCE, Linode.com
		


3. Завершение.

	- Работа с инфраструктурой должна перестать быть тайными знаниями инженеров.

	- Формализованный в виде кода подход к работе с инфраструктурой помагает упростить процесс разработки, тестирования и доставки ПО.

	- IAC неотъемлемая и важная часть современных IT проектов


